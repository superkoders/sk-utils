// eslint-disable-next-line no-undef
module.exports = {
	extends: ['@superkoders/stylelint-config', 'stylelint-config-prettier'],
	plugins: ['stylelint-prettier'],
	rules: {
		'prettier/prettier': true,
	},
};
