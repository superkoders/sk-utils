module.exports = {
	useTabs: true,
	semi: true,
	singleQuote: true,
	trailingComma: 'all',
	// tabWidth: 4
	// quoteProps: 'consistent',
	// jsxSingleQuote: false,
	// bracketSpacing: true,
	// jsxBracketSameLine: true,
	// arrowParens: 'always',
	// parser: 'typescript',
};
