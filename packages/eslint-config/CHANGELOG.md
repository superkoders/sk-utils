# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.1.1](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@2.1.0...@superkoders/eslint-config@2.1.1) (2022-02-17)


### Bug Fixes

* eslint config and dependencies ([1beec66](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/commits/1beec66e71228755e89200d93243aa57c16a583c))





# [2.1.0](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@2.0.0...@superkoders/eslint-config@2.1.0) (2022-02-17)


### Bug Fixes

* eslint config and dependencies ([f25731e](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/commits/f25731e97a9cd3d1cd6b7e79b861ef9d7c851c28))





# [2.0.0](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.25...@superkoders/eslint-config@2.0.0) (2022-02-17)

**Note:** Version bump only for package @superkoders/eslint-config






## [1.2.25](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.24...@superkoders/eslint-config@1.2.25) (2020-03-13)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.24](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.23...@superkoders/eslint-config@1.2.24) (2020-03-10)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.23](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.22...@superkoders/eslint-config@1.2.23) (2020-03-10)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.22](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.21...@superkoders/eslint-config@1.2.22) (2020-03-06)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.21](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.20...@superkoders/eslint-config@1.2.21) (2020-03-05)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.20](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.19...@superkoders/eslint-config@1.2.20) (2020-03-05)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.19](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.18...@superkoders/eslint-config@1.2.19) (2020-02-27)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.18](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.17...@superkoders/eslint-config@1.2.18) (2020-02-15)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.17](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.16...@superkoders/eslint-config@1.2.17) (2020-02-11)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.16](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.15...@superkoders/eslint-config@1.2.16) (2020-01-23)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.15](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.14...@superkoders/eslint-config@1.2.15) (2020-01-16)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.14](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.13...@superkoders/eslint-config@1.2.14) (2020-01-15)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.13](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.12...@superkoders/eslint-config@1.2.13) (2020-01-10)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.12](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.11...@superkoders/eslint-config@1.2.12) (2020-01-10)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.11](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.10...@superkoders/eslint-config@1.2.11) (2020-01-09)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.10](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.9...@superkoders/eslint-config@1.2.10) (2020-01-09)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.9](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.8...@superkoders/eslint-config@1.2.9) (2019-12-12)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.8](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.7...@superkoders/eslint-config@1.2.8) (2019-12-12)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.7](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.6...@superkoders/eslint-config@1.2.7) (2019-12-11)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.6](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.5...@superkoders/eslint-config@1.2.6) (2019-12-11)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.5](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.4...@superkoders/eslint-config@1.2.5) (2019-12-11)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.4](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.3...@superkoders/eslint-config@1.2.4) (2019-11-28)


### Bug Fixes

* remove period ([6b02fa8](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/commits/6b02fa8ed21a37a7b0650b5434a62e201b2dcc27))





## [1.2.3](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.2...@superkoders/eslint-config@1.2.3) (2019-11-28)

**Note:** Version bump only for package @superkoders/eslint-config





## [1.2.2](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.1...@superkoders/eslint-config@1.2.2) (2019-11-28)


### Bug Fixes

* bump version so I can check release ([4f1c3a2](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/commits/4f1c3a225e418498f3bc5c62977119e1e8da82b4))





## [1.2.1](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.2.0...@superkoders/eslint-config@1.2.1) (2019-11-28)

**Note:** Version bump only for package @superkoders/eslint-config





# [1.2.0](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/compare/@superkoders/eslint-config@1.1.0...@superkoders/eslint-config@1.2.0) (2019-11-28)


### Features

* update eslint docs ([8692567](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/commits/8692567563b48d80767c882de42a0abc3b487fa7))





# 1.1.0 (2019-11-28)


### Features

* **eslint-config:** add eslint config ([665ccd9](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-config/commits/665ccd96ecd83c868880c262b84bed089fa57f7e))
