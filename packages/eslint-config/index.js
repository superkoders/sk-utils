module.exports = {
	extends: ['eslint:recommended', 'plugin:prettier/recommended'],
	globals: {
		PROJECT_CONFIG: 'readonly',
	},
	plugins: ['@babel'],
	parser: '@babel/eslint-parser',
	parserOptions: {
		ecmaVersion: 2020,
		sourceType: 'module',
	},
	env: {
		browser: true,
		node: true,
		commonjs: true,
		es6: true,
	},
	rules: {
		'prettier/prettier': ['warn'],
		'@babel/no-unused-expressions': 'error',
		'linebreak-style': 'off',
	},
};
