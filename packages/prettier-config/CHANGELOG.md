# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.6](https://bitbucket.org/superkoders/sk-utils/src/master/packages/prettier-config/compare/@superkoders/prettier-config@0.2.5...@superkoders/prettier-config@0.2.6) (2020-03-25)

**Note:** Version bump only for package @superkoders/prettier-config





## [0.2.5](https://bitbucket.org/superkoders/sk-utils/src/master/packages/prettier-config/compare/@superkoders/prettier-config@0.2.4...@superkoders/prettier-config@0.2.5) (2020-03-04)

**Note:** Version bump only for package @superkoders/prettier-config





## [0.2.4](https://bitbucket.org/superkoders/sk-utils/src/master/packages/prettier-config/compare/@superkoders/prettier-config@0.2.3...@superkoders/prettier-config@0.2.4) (2020-03-03)

**Note:** Version bump only for package @superkoders/prettier-config





## [0.2.3](https://bitbucket.org/superkoders/sk-utils/src/master/packages/prettier-config/compare/@superkoders/prettier-config@0.2.2...@superkoders/prettier-config@0.2.3) (2020-03-03)

**Note:** Version bump only for package @superkoders/prettier-config





## [0.2.2](https://bitbucket.org/superkoders/sk-utils/src/master/packages/prettier-config/compare/@superkoders/prettier-config@0.2.1...@superkoders/prettier-config@0.2.2) (2020-03-03)

**Note:** Version bump only for package @superkoders/prettier-config





## [0.2.1](https://bitbucket.org/superkoders/sk-utils/src/master/packages/prettier-config/compare/@superkoders/prettier-config@0.2.0...@superkoders/prettier-config@0.2.1) (2020-02-11)

**Note:** Version bump only for package @superkoders/prettier-config





# 0.2.0 (2020-01-16)


### Features

* add shareable prettier config ([ebc9274](https://bitbucket.org/superkoders/sk-utils/src/master/packages/prettier-config/commits/ebc927402296d793f66e6764929045c79fc277cf))
