# @superkoders/prettier-config

SK shareable prettier config, which is prepared for use with [our eslint configuration](https://www.npmjs.com/package/@superkoders/eslint-config)

## Instalation

### 1. Install the package
```bash
npm i -D @superkoders/prettier-config
```

### 2. Add .prettierrc.js
This tells prettier where to locate our rules. You can also override the rules here, if you have some exception on a given project.
```js
module.exports = {
	...require('@superkoders/prettier-config'),
};
```

If you need to make special overrides for given project, you can do so like this:
```js
module.exports = {
	...require('@superkoders/prettier-config'),
	useTabs: true // you can add overrides like so
};
```

### 3. Add .prettierignore
Specify paths and files you don't want to autoformat. Be sure to ignore minified 3rd party files. Good starting point might be this:
```yaml
*.md
*.mdown
*.mdx
package.json
package-lock.json
web/*
src/js/static/*
**/node_modules/**/*
```

### 4. Add .gitattributes file
Make sure, that line endings will be same accross platforms / OS. This line will make the magic:
```yaml
* text eol=lf
```


### 5. Optional: Live error/warning highlightning with editor extension
Download an extension, which will highlight problematic code and give you options how to fix it or which will auto format it as you save.
<a href="https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode" target="_blank" rel="noreferrer noopener">Prettier do VS Code</a>


### 6. Update VS Code settings for autoformatting
Ideally, save those settings in `.vscode/settings.json` so it lives with the project, if it isn't there already.
```json
"editor.formatOnSave": true,
"prettier.requireConfig": true, // Only format configured projects
```
