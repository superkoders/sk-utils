// eslint-disable-next-line no-undef
module.exports = {
	extends: ['@commitlint/config-conventional'],
	rules: {
		'body-leading-blank': [2, 'always'],
		'header-max-length': [0, 'always', 72],
		'type-enum': [
			2,
			'always',
			[
				'feat',
				'fix',
				'tpl',
				'ui',
				'content',
				'refactor',
				'docs',
				'style',
				'perf',
				'test',
				'build',
				'ci',
				'config',
				'chore',
				'revert',
			],
		],
	},
};
