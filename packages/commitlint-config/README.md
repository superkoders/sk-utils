# @superkoders/commitlint-config
Our custom commitlint rules.

## Instalation

### 1. Install the package
```bash
npm i -D @superkoders/commitlint-config
```

### 2. Add commitlint.config.js
This tells commitlint when to locate our rules. You can also override the rules here, if you have some exception on a given project.
```json
module.exports = {
	extends: ["@superkoders/commitlint-config"],
};
```

### 3. Install husky - git hooks utility
```bash
npm i -D husky
```

### 4. Set up husky hook
Add to the package.json
```json
{
	"husky": {
		"hooks": {
			"commit-msg": "commitlint -E HUSKY_GIT_PARAMS"
		}
	},
}
```

## Commit Keywords
When commiting, we stick with conventional config settings. Permitted keywords are those:
- **feat:** add or extend backwards compatible functionality. It bumps version by *minor* and reset patch number to 0.
- **fix:** bugfixes, it bumps version by *patch*.
- **refactor:** code changes, which don't fix anything nor adds new functionality
- **perf:** performance related changes
- **tpl:** update templates
- **revert:** for reverting commits

- **ui:** UI adjustments
- **style:** code changes, which don't alter its function (eg. formatting)
- **content:** Images, text edits and alike
- **docs:** only documentation changes
- **test:** add or edit tests
- **build:** changes related to project build (eg. webpack)
- **ci:** changes related to project integration (eg. CI)
- **config:** Config and rules changes
- **chore:** other changes, which don't alter source or test code (eg. release new version)
- when introducing backwards incompatible API changes (which bumps version by **MAJOR**), indicate it with keyword `BREAKING CHANGE` written at the very beginning of commit body, ideally with additional information. Commit subject should be as usual - ketyword and explanation. Eg.
```bash
refactor: unify componentA and componentB, change input data

BREAKING CHANGE here can be another explanation and reasoning
```

## Commit formatting rules
- subject always starts with small letter
- empty line between subject and body is mandatory

For more on commitlint visit <a href="https://commitlint.js.org/" target="_blank" rel="noreferrer noopener">official documentation</a>.
