# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.3](https://bitbucket.org/superkoders/sk-utils/src/master/packages/commitlint-config/compare/@superkoders/commitlint-config@2.0.2...@superkoders/commitlint-config@2.0.3) (2020-01-23)

**Note:** Version bump only for package @superkoders/commitlint-config





## [2.0.2](https://bitbucket.org/superkoders/sk-utils/src/master/packages/commitlint-config/compare/@superkoders/commitlint-config@2.0.1...@superkoders/commitlint-config@2.0.2) (2020-01-10)

**Note:** Version bump only for package @superkoders/commitlint-config





## [2.0.1](https://bitbucket.org/superkoders/sk-utils/src/master/packages/commitlint-config/compare/@superkoders/commitlint-config@2.0.0...@superkoders/commitlint-config@2.0.1) (2020-01-10)

**Note:** Version bump only for package @superkoders/commitlint-config





# [2.0.0](https://bitbucket.org/superkoders/sk-utils/src/master/packages/commitlint-config/compare/@superkoders/commitlint-config@1.2.3...@superkoders/commitlint-config@2.0.0) (2020-01-03)


### Code Refactoring

* remove lerna-scopes ([59f157f](https://bitbucket.org/superkoders/sk-utils/src/master/packages/commitlint-config/commits/59f157ffe1c0c6df057e246685eb330ebe1bcafd))


### BREAKING CHANGES

* it prevented linting on repos without lerna packages, so I opted to put it away





## [1.2.3](https://bitbucket.org/superkoders/sk-utils/src/master/packages/commitlint-config/compare/@superkoders/commitlint-config@1.2.2...@superkoders/commitlint-config@1.2.3) (2019-12-12)

**Note:** Version bump only for package @superkoders/commitlint-config





## [1.2.2](https://bitbucket.org/superkoders/sk-utils/src/master/packages/commitlint-config/compare/@superkoders/commitlint-config@1.2.1...@superkoders/commitlint-config@1.2.2) (2019-12-12)

**Note:** Version bump only for package @superkoders/commitlint-config





## [1.2.1](https://bitbucket.org/superkoders/sk-utils/src/master/packages/commitlint-config/compare/@superkoders/commitlint-config@1.2.0...@superkoders/commitlint-config@1.2.1) (2019-12-12)

**Note:** Version bump only for package @superkoders/commitlint-config





# [1.2.0](https://bitbucket.org/superkoders/sk-utils/src/master/packages/commitlint-config/compare/@superkoders/commitlint-config@1.1.3...@superkoders/commitlint-config@1.2.0) (2019-12-11)


### Features

* add 'config' type to commitlint types ([e42fee7](https://bitbucket.org/superkoders/sk-utils/src/master/packages/commitlint-config/commits/e42fee72ff8ac87faed1cc9dc7d5a5d4ca52217b))





## [1.1.3](https://bitbucket.org/superkoders/sk-utils/src/master/packages/commitlint-config/compare/@superkoders/commitlint-config@1.1.2...@superkoders/commitlint-config@1.1.3) (2019-12-11)

**Note:** Version bump only for package @superkoders/commitlint-config





## [1.1.2](https://bitbucket.org/superkoders/sk-utils/src/master/packages/commitlint-config/compare/@superkoders/commitlint-config@1.1.1...@superkoders/commitlint-config@1.1.2) (2019-12-11)

**Note:** Version bump only for package @superkoders/commitlint-config





## [1.1.1](https://bitbucket.org/superkoders/sk-utils/src/master/packages/commitlint-config/compare/@superkoders/commitlint-config@1.1.0...@superkoders/commitlint-config@1.1.1) (2019-11-28)

**Note:** Version bump only for package @superkoders/commitlint-config





# 1.1.0 (2019-11-28)


### Bug Fixes

* **commitlint-config:** fix repo url ([4c953e1](https://bitbucket.org/superkoders/sk-utils/src/master/packages/commitlint-config/commits/4c953e13b27bb8389871878f498e243f068c0fb1))
* **commitlint-config:** fix repo url ([08ed4fd](https://bitbucket.org/superkoders/sk-utils/src/master/packages/commitlint-config/commits/08ed4fdc273d4c9a7c4a7433d1770c27b195a827))
* **commitlint-config:** provide right repo url ([73f7af5](https://bitbucket.org/superkoders/sk-utils/src/master/packages/commitlint-config/commits/73f7af54d511697879478ac8d7d7bb0ab4a99506))


### Features

* **commitlint-config:** Add commitlint config ([8886380](https://bitbucket.org/superkoders/sk-utils/src/master/packages/commitlint-config/commits/8886380ae6d3e679623e2bb427f488366508f693))





# 1.0.0

-   Initial release
