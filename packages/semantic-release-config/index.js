// eslint-disable-next-line no-undef
module.exports = {
	plugins: [
		[
			'@semantic-release/commit-analyzer',
			{
				preset: 'conventionalcommits',
				releaseRules: [
					{ type: 'feat', release: 'minor' },
					{ type: 'fix', release: 'patch' },
					{ type: 'refactor', release: 'patch' },
					{ type: 'perf', release: 'patch' },
					{ type: 'tpl', release: 'patch' },
					{ type: 'revert', release: 'patch' },
				],
			},
		],
		[
			'@semantic-release/release-notes-generator',
			{
				preset: 'conventionalcommits',
				presetConfig: {
					header: 'SK',
					types: [
						{ type: 'feat', section: 'Features' },
						{ type: 'fix', section: 'Bug Fixes' },
						{ type: 'refactor', section: 'Existing code adjustments' },
						{ type: 'perf', section: 'Performance improvements' },
						{ type: 'tpl', section: 'Template updates' },
						{ type: 'revert', section: 'Reverted code' },
						{ type: 'content', hidden: true },
						{ type: 'config', hidden: true },
						{ type: 'ui', hidden: true },
						{ type: 'docs', hidden: true },
						{ type: 'style', hidden: true },
						{ type: 'chore', hidde: true },
						{ type: 'test', hidden: true },
					],
				},
			},
		],
		[
			'@semantic-release/npm',
			{
				npmPublish: false,
			},
		],
		'@semantic-release/changelog',
		[
			'@semantic-release/git',
			{
				message:
					'chore(release): ${nextRelease.version} \n\n${nextRelease.notes}',
			},
		],
	],
	branch: 'master',
	// eslint-disable-next-line no-template-curly-in-string
	tagFormat: 'v${version}',
};
