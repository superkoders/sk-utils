# @superkoders/semantic-release-config
Semantic-release-config complementing our <a href="https://www.npmjs.com/package/@superkoders/commitlint-config" target="_blank" rel="noreferrer noopener">custom commitlint rules</a>.

## Instalation

### 1. Install the package
```bash
npm i -D @superkoders/semantic-release-config
```

### 2. Add .releaserc
This tells semantic-release tool when to locate our rules. You can also override the rules here, if you have some exception on a given project. And on which branch it is possible to make release.
```json
{
	"extends": [
		"@superkoders/semantic-release-config"
	],
	"branch": "master"
}
```

### 3. How to run release
There are many ways and choosing one depends on the context.
1. test the function `npx semantic-release --dry-mode`
2. manually by calling `npx semantic-release --no-ci`
3. automatically as a part of pipeline. This requires setting up the pipeline and also some sort of git flow, so the release is triggered at the right moment.

## More information
For more on semantic release visit <a href="https://semantic-release.gitbook.io/semantic-release/" target="_blank" rel="noreferrer noopener">official documentation</a>.
