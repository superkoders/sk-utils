# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.6](https://bitbucket.org/superkoders/sk-utils/src/master/packages/semantic-release-config/compare/@superkoders/semantic-release-config@1.1.5...@superkoders/semantic-release-config@1.1.6) (2020-04-24)

**Note:** Version bump only for package @superkoders/semantic-release-config





## [1.1.5](https://bitbucket.org/superkoders/sk-utils/src/master/packages/semantic-release-config/compare/@superkoders/semantic-release-config@1.1.4...@superkoders/semantic-release-config@1.1.5) (2020-01-23)

**Note:** Version bump only for package @superkoders/semantic-release-config





## [1.1.4](https://bitbucket.org/superkoders/sk-utils/src/master/packages/semantic-release-config/compare/@superkoders/semantic-release-config@1.1.3...@superkoders/semantic-release-config@1.1.4) (2020-01-10)

**Note:** Version bump only for package @superkoders/semantic-release-config





## [1.1.3](https://bitbucket.org/superkoders/sk-utils/src/master/packages/semantic-release-config/compare/@superkoders/semantic-release-config@1.1.2...@superkoders/semantic-release-config@1.1.3) (2020-01-10)

**Note:** Version bump only for package @superkoders/semantic-release-config





## [1.1.2](https://bitbucket.org/superkoders/sk-utils/src/master/packages/semantic-release-config/compare/@superkoders/semantic-release-config@1.1.1...@superkoders/semantic-release-config@1.1.2) (2020-01-09)

**Note:** Version bump only for package @superkoders/semantic-release-config





## [1.1.1](https://bitbucket.org/superkoders/sk-utils/src/master/packages/semantic-release-config/compare/@superkoders/semantic-release-config@1.1.0...@superkoders/semantic-release-config@1.1.1) (2019-12-12)

**Note:** Version bump only for package @superkoders/semantic-release-config





# 1.1.0 (2019-12-12)


### Features

* **semantic-release-config:** add shareable configuration ([0d81164](https://bitbucket.org/superkoders/sk-utils/src/master/packages/semantic-release-config/commits/0d81164e000febd6d72cae2105298de4f9052b2a))
