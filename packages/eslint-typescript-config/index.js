const base = require('@superkoders/eslint-config');

module.exports = {
	extends: ['@superkoders/eslint-config'],
	settings: {
		'import/resolver': {
			node: {
				extensions: [
					'.ts',
					'.tsx',
					'.d.ts',
					...base.settings['import/resolver'].node.extensions,
				],
			},
			typescript: {},
		},
	},
	globals: {
		PROJECT_CONFIG: 'readonly',
	},
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaVersion: 2020,
		sourceType: 'module',
	},
	env: {
		browser: true,
		node: true,
		commonjs: true,
		es6: true,
	},
	rules: {
		'valid-jsdoc': 'off',
		'no-dupe-class-members': 'off',
		'no-undef': 'off',
		'no-unused-vars': 'off',
		'no-useless-constructor': 'off',
		'@typescript-eslint/adjacent-overload-signatures': 'warn',
		'@typescript-eslint/array-type': [
			'warn',
			{
				default: 'array-simple',
				readonly: 'array-simple',
			},
		],
		'@typescript-eslint/await-thenable': 'warn',
		'@typescript-eslint/ban-ts-comment': 'warn',
		'@typescript-eslint/explicit-function-return-type': [
			'warn',
			{
				allowExpressions: true,
			},
		],
		'@typescript-eslint/explicit-member-accessibility': [
			'warn',
			{
				accessibility: 'no-public',
			},
		],
		'@typescript-eslint/explicit-module-boundary-types': 'warn',
		'@typescript-eslint/member-ordering': 'warn',
		'@typescript-eslint/no-array-constructor': 'error',
		'@typescript-eslint/no-dupe-class-members':
			base.rules['no-dupe-class-members'],
		'@typescript-eslint/no-dynamic-delete': 'warn',
		'@typescript-eslint/no-empty-interface': 'warn',
		'@typescript-eslint/no-explicit-any': 'warn',
		'@typescript-eslint/no-extra-semi': base.rules['no-extra-semi'],
		'@typescript-eslint/no-extraneous-class': [
			'warn',
			{
				allowConstructorOnly: true,
				allowWithDecorator: true,
			},
		],
		'@typescript-eslint/no-floating-promises': 'warn',
		'@typescript-eslint/no-for-in-array': 'warn',
		'@typescript-eslint/no-implied-eval': base.rules['no-implied-eval'],
		'@typescript-eslint/no-misused-new': 'warn',
		'@typescript-eslint/no-misused-promises': 'warn',
		'@typescript-eslint/no-namespace': 'error',
		'@typescript-eslint/no-non-null-asserted-optional-chain': 'warn',
		'@typescript-eslint/no-non-null-assertion': 'warn',
		'@typescript-eslint/no-parameter-properties': 'warn',
		'@typescript-eslint/no-require-imports': 'warn',
		'@typescript-eslint/no-this-alias': [
			'warn',
			{
				allowDestructuring: true,
			},
		],
		'@typescript-eslint/no-throw-literal': base.rules['no-throw-literal'],
		'@typescript-eslint/no-unnecessary-condition': 'warn',
		'@typescript-eslint/no-unnecessary-qualifier': 'warn',
		'@typescript-eslint/no-unnecessary-type-assertion': 'warn',
		'@typescript-eslint/triple-slash-reference': [
			'error',
			{
				path: 'never',
				types: 'never',
				lib: 'never',
			},
		],
		'@typescript-eslint/no-unused-vars': base.rules['no-unused-vars'],
		'@typescript-eslint/no-use-before-define': [
			'error',
			{
				functions: false,
				classes: false,
				typedefs: false,
			},
		],
		'@typescript-eslint/no-var-requires': 'error',
		'@typescript-eslint/no-useless-constructor':
			base.rules['no-useless-constructor'],
		'@typescript-eslint/prefer-namespace-keyword': 'warn',
		'@typescript-eslint/promise-function-async': 'warn',
		'@typescript-eslint/require-array-sort-compare': 'warn',
		'@typescript-eslint/require-await': base.rules['require-await'],
		'@typescript-eslint/restrict-plus-operands': 'warn',
		'@typescript-eslint/restrict-template-expressions': 'warn',
		'@typescript-eslint/return-await': ['warn', 'always'],
		'@typescript-eslint/switch-exhaustiveness-check': 'warn',
		'@typescript-eslint/unbound-method': 'warn',
	},
};
