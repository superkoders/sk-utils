# @superkoders/eslint-config

## Instalation
In case you have a Typescript project, use this package instead of `@superkoders/eslint-config`. This package is extending it with some rules for Typescript.

### 1. Install the package
```bash
npm i -D @superkoders/eslint-typescript-config
```

### 2. Add .eslintrc.js
This tells linter where to locate our rules. You can also override the rules here, if you have some exception on a given project.
```js
module.exports = {
	'extends': ['@superkoders/eslint-typescript-config']
};
```

### 3. Add `.eslintignore`
Package.json and package-lock.json is generated automatically. It might be problematic to enforce styling on this one. Use your own config, which might contain things like those:
```yaml
# Of particular note is that like .gitignore files, all paths used as patterns for both .eslintignore and --ignore-pattern must use forward slashes as their path separators.
# /node_modules/* and /bower_components/* in the project root are ignored by default
web/**/*
src/js/static/**/*
```

### 4. Add Prettier
Prettier comes with some opinions by default, which can conflict with other linters' settings. This will make sure they're in sync. Docs on how to [download and setup SK Prettier config](https://www.npmjs.com/package/@superkoders/prettier-config).

### 5. Add lint script to the package.json
And adjust the paths to your project needs.
```json
{
	"scripts": {
		"lint:js": "eslint .; exit 0",
		"lint:js:fix": "eslint . --fix",
	},
}
```

### 6. Optional: Install git hooks utility
If you want to prevent commiting bad code, you can let it check before commiting. If not right, it will notify the developer. When the code is correct, it will get a green light to the repo.
```bash
npm i -D husky lint-staged
```

### 7. Set up the pre-commit hook
Add to the package.json
```json
"husky": {
	"hooks": {
		"pre-commit": "lint-staged"
	}
},
"lint-staged": {
	"*.js": [
		"eslint"
	]
},
```
Running `npm run lint-js` will check the code and output list of errors and warnings to the console.
Running `npm run lint-js:fix` will check and fix the code. This is omst useful when introducing linter to existing codebase.

### 8. Optional: Live error/warning highlightning with editor extension
Download an extension, which will highlight problematic code and give you options how to fix it or which will auto format it as you save.
<a href="https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode" target="_blank" rel="noreferrer noopener">Prettier do VS Code</a>
<a href="https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint" target="_blank" rel="noreferrer noopener">ESLint do VS Code</a>

### 9. Update VS Code settings for autoformatting
Ideally, save those settings in `.vscode/settings.json` so it lives with the project, if it isn't there already.
```json
"editor.formatOnSave": true,
"editor.codeActionsOnSave": {
	"source.fixAll.eslint": true,
},
"prettier.requireConfig": true, // Only format configured projects
```
