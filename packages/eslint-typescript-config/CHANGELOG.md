# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.1](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-typescript-config/compare/@superkoders/eslint-typescript-config@1.1.0...@superkoders/eslint-typescript-config@1.1.1) (2022-02-17)

**Note:** Version bump only for package @superkoders/eslint-typescript-config





# [1.1.0](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-typescript-config/compare/@superkoders/eslint-typescript-config@1.0.0...@superkoders/eslint-typescript-config@1.1.0) (2022-02-17)

**Note:** Version bump only for package @superkoders/eslint-typescript-config





# [1.0.0](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-typescript-config/compare/@superkoders/eslint-typescript-config@0.2.6...@superkoders/eslint-typescript-config@1.0.0) (2022-02-17)

**Note:** Version bump only for package @superkoders/eslint-typescript-config






## [0.2.6](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-typescript-config/compare/@superkoders/eslint-typescript-config@0.2.5...@superkoders/eslint-typescript-config@0.2.6) (2020-03-13)

**Note:** Version bump only for package @superkoders/eslint-typescript-config





## [0.2.5](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-typescript-config/compare/@superkoders/eslint-typescript-config@0.2.4...@superkoders/eslint-typescript-config@0.2.5) (2020-03-10)

**Note:** Version bump only for package @superkoders/eslint-typescript-config





## [0.2.4](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-typescript-config/compare/@superkoders/eslint-typescript-config@0.2.3...@superkoders/eslint-typescript-config@0.2.4) (2020-03-10)

**Note:** Version bump only for package @superkoders/eslint-typescript-config





## [0.2.3](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-typescript-config/compare/@superkoders/eslint-typescript-config@0.2.2...@superkoders/eslint-typescript-config@0.2.3) (2020-03-06)

**Note:** Version bump only for package @superkoders/eslint-typescript-config





## [0.2.2](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-typescript-config/compare/@superkoders/eslint-typescript-config@0.2.1...@superkoders/eslint-typescript-config@0.2.2) (2020-03-05)

**Note:** Version bump only for package @superkoders/eslint-typescript-config





## [0.2.1](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-typescript-config/compare/@superkoders/eslint-typescript-config@0.2.0...@superkoders/eslint-typescript-config@0.2.1) (2020-03-05)

**Note:** Version bump only for package @superkoders/eslint-typescript-config





# 0.2.0 (2020-03-03)


### Features

* typescript eslint config package ([82c856a](https://bitbucket.org/superkoders/sk-utils/src/master/packages/eslint-typescript-config/commits/82c856aac92cf4dd3873a5bf1436273ac5e9d9f3))
