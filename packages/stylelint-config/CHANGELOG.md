# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.2.2](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@2.2.1...@superkoders/stylelint-config@2.2.2) (2024-03-28)


### Bug Fixes

* update prettier package ([6ba3afc](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/6ba3afca0891316f8222bdbc4605e2e685825525))





## [2.2.1](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@2.2.0...@superkoders/stylelint-config@2.2.1) (2024-03-28)

**Note:** Version bump only for package @superkoders/stylelint-config





# [2.2.0](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@2.1.0...@superkoders/stylelint-config@2.2.0) (2023-12-05)


### Features

* add length-zero-no-unit rule for stylelint-config ([75e1624](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/75e162409e9a488282d33f12693ea85097552c7b))





# [2.1.0](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@2.0.0...@superkoders/stylelint-config@2.1.0) (2022-01-11)


### Features

* add keyframes-name-pattern rule ([3c7efcd](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/3c7efcdb90c935430eb59ab13a501e0e800276b9))






# [2.0.0](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.5.0...@superkoders/stylelint-config@2.0.0) (2021-11-04)


### chore

* bump version ([a2953d9](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/a2953d9e695164cbe625a5f1cd7beb583f6684f6))


### Reverts

* Revert "Publish" ([efb483d](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/efb483d5f81ee9db8f90942ea9b19d915e85a833))


### BREAKING CHANGES

* well, I forgot to add this magic word in previous commit





# [1.4.0](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.2.4...@superkoders/stylelint-config@1.4.0) (2021-09-09)


### Bug Fixes

* Bump up stylelint version ([de807a0](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/de807a00c0eace9af1e4ef53e1e9c55f1606a80c))
* disable no-descending-specificity and scss/double-slash-comment-empty-line-before ([c9705d3](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/c9705d365e9d1e6cbd2a51c01c9922092eeaf650))


### Features

* Ignore pseudoselectors :is, :matches ([dc3068a](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/dc3068a4010dcf405fad4d362b15d0ec718a10dd))





## [1.2.4](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.2.3...@superkoders/stylelint-config@1.2.4) (2020-03-13)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.2.3](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.2.2...@superkoders/stylelint-config@1.2.3) (2020-03-10)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.2.2](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.2.1...@superkoders/stylelint-config@1.2.2) (2020-03-06)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.2.1](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.2.0...@superkoders/stylelint-config@1.2.1) (2020-03-06)


### Bug Fixes

* Multiline comments does not require new line before ([583940c](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/583940cf6e82c7b7325815eb7c25644224c85026))





# [1.2.0](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.23...@superkoders/stylelint-config@1.2.0) (2020-03-06)


### Features

* Handle empty lines ([52748a5](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/52748a5d9b0a2ccc10035af01cf0e62b8e3c05f4))
* Remove SCSS mixin and variable duplicities ([61642bd](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/61642bdbe69ca3ea8927cbc47bd6e3ab038549b5))





## [1.1.23](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.22...@superkoders/stylelint-config@1.1.23) (2020-03-04)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.22](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.21...@superkoders/stylelint-config@1.1.22) (2020-03-03)


### Bug Fixes

* Remove duplicates with sytlelint-config-standard package ([fc33513](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/fc33513f87285ed8c9fb904e8f4c32f2cac22776))





## [1.1.21](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.20...@superkoders/stylelint-config@1.1.21) (2020-03-03)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.20](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.19...@superkoders/stylelint-config@1.1.20) (2020-02-27)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.19](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.18...@superkoders/stylelint-config@1.1.19) (2020-02-27)


### Bug Fixes

* Group stylelint rules by the name for better readibility ([9220eb5](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/9220eb5ea6987cc7f22910d57a6824c9e1ec3845))





## [1.1.18](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.17...@superkoders/stylelint-config@1.1.18) (2020-02-26)


### Bug Fixes

* Use single quotes & remove empty line after @ rule ([ce90bb8](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/ce90bb8ba9df63260cc28df425ca8023b033ef7d))





## [1.1.17](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.16...@superkoders/stylelint-config@1.1.17) (2020-02-24)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.16](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.15...@superkoders/stylelint-config@1.1.16) (2020-02-16)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.15](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.14...@superkoders/stylelint-config@1.1.15) (2020-02-15)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.14](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.13...@superkoders/stylelint-config@1.1.14) (2020-01-23)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.13](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.12...@superkoders/stylelint-config@1.1.13) (2020-01-16)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.12](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.11...@superkoders/stylelint-config@1.1.12) (2020-01-15)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.11](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.10...@superkoders/stylelint-config@1.1.11) (2020-01-10)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.10](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.9...@superkoders/stylelint-config@1.1.10) (2020-01-10)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.9](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.8...@superkoders/stylelint-config@1.1.9) (2020-01-09)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.8](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.7...@superkoders/stylelint-config@1.1.8) (2020-01-09)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.7](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.6...@superkoders/stylelint-config@1.1.7) (2020-01-09)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.6](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.5...@superkoders/stylelint-config@1.1.6) (2019-12-12)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.5](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.4...@superkoders/stylelint-config@1.1.5) (2019-12-12)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.4](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.3...@superkoders/stylelint-config@1.1.4) (2019-12-11)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.3](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.2...@superkoders/stylelint-config@1.1.3) (2019-12-11)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.2](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.1...@superkoders/stylelint-config@1.1.2) (2019-12-11)

**Note:** Version bump only for package @superkoders/stylelint-config





## [1.1.1](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/compare/@superkoders/stylelint-config@1.1.0...@superkoders/stylelint-config@1.1.1) (2019-11-28)

**Note:** Version bump only for package @superkoders/stylelint-config





# 1.1.0 (2019-11-28)


### Bug Fixes

* **stylelint-config:** fix dependencyies ([c749f63](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/c749f635dbb2a04cd29acd6dfd70d91969aedb0e))
* **stylelint-config:** fix repo url ([87cedcd](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/87cedcd62d06ee39958679f6abd5af314cb45f4f))
* **stylelint-config:** provide right repo url ([71e50ef](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/71e50efed8daae768b6d3094eccd6e9f8a0de658))


### Features

* add stylelint config ([69cdf33](https://bitbucket.org/superkoders/sk-utils/src/master/packages/stylelint-config/commits/69cdf331bd8188f1c59d8cd4ce6416fd76e69971))





# 1.0.0

-   Initial release
