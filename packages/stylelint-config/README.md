# @superkoders/stylelint-config

Stylelint set the rules how to keep your project's SCSS nice and tidy.

## Instalation

### 1. Install the package
```bash
npm i -D @superkoders/stylelint-config
```

### 2. Add .stylelintrc.js
This tells linter where to locate our rules. You can also override the rules here, if you have some exception on a given project.
```js
module.exports = {
	extends: ['@superkoders/stylelint-config', 'stylelint-prettier/recommended'],
};
```

### 3. Add .stylelintignore
Specify paths and files you don't want to autoformat. Good starting point might be this:
```yaml
bin
obj
*.*
!*.css
!*.scss
**/node_modules/**/*
```

### 4. Add Prettier
Prettier comes with some opinions by default, which can conflict with other linters' settings. This will make sure they're in sync. Docs on how to [download and setup SK Prettier config](https://www.npmjs.com/package/@superkoders/prettier-config).

### 5. Add lint script to the package.json
And adjust the paths to your project needs.
```json
{
	"scripts": {
		"lint:css": "stylelint --syntax scss \"src/**/*.{css,scss}\"; exit 0",
		"lint:css:fix": "prettier-stylelint --write \"src/**/*.{css,scss}\"",
	},
}
```

### 6. Optional: Install git hooks utility
If you want to prevent commiting bad code, you can let it check before commiting. If not right, it will notify the developer. When the code is correct, it will get a green light to the repo.
```bash
npm i -D husky lint-staged
```

### 7. Set up the pre-commit hook
Add to the package.json
```json
"husky": {
	"hooks": {
		"pre-commit": "lint-staged"
	}
},
"lint-staged": {
	"*.{scss}": [
		"stylelint --syntax scss \"*.{css,scss}\""
	]
},
```
Running `npm run lint:css` will check the code and output list of errors and warnings to the console.
Running `npm run lint:css:fix` will check and fix the code. This is omst useful when introducing linter to existing codebase.

### 8. Optional: Live error/warning highlightning with editor extensions
Download an extension, which will highlight problematic code and give you options how to fix it or which will auto format it as you save.
<a href="https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode" target="_blank" rel="noreferrer noopener">Prettier for VS Code</a>
<a href="https://marketplace.visualstudio.com/items?itemName=stylelint.vscode-stylelint" target="_blank" rel="noreferrer noopener">Stylelint for VS Code</a>

### 9. Update VS Code settings for autoformatting
Ideally, save those settings in `.vscode/settings.json` so it lives with the project, if it isn't there already.
```json
"editor.formatOnSave": true,
"editor.codeActionsOnSave": {
	"source.fixAll.stylelint": true
},
"prettier.requireConfig": true, // Only format configured projects
```

## Our lint rules and reasoning behind them

### Order

#### Order of declaration blocks:
1. custom-properties - CSS variables - so they're available
2. dollar-variables - SCSS variables - so they're available
3. mixins, extends without blocks (simple mixins) - it should go first, so it can be overriden if needed
4. declarations - like `display: flex;` - the main styles
5. mixins with blocks - extensions of the code like media queries, althout we opt to write all media queries to the bottom of the file, rather than keep it coupled with the code blocks
6. rules - nested rules, like `.class { span { ... } }` - if there needs to be done nested styling, we do it at the bottom of the rule.

#### Order of the rules
Purpose of ordering rules is to help us quickly scan the code. We want consistend rule blocks while keeping relevant rules together. That's why I opted for grouping based on box-model. We go from outside-in.
We start with layout, box-sizing to position rules, flex, margins, borders, paddings. When specifing e.g. margins one by one, it will be sorted in this order: top, right, bottom, left.
You should be able quickly find what you are looking for or take a look in the place where you expect it. If it is not there, you know it is missing and don't have to scan whole ruleset.

### Rules
Set of rules which extends our CSS convention based on [Idomatic CSS Principles](https://github.com/necolas/idiomatic-css/)

#### Functional**
- **selector-no-qualifying-type: true, [...]** adds unnecessary specificity and lowers reusability
- **no-duplicate-selectors: true**, we don't want rules belonging to one selector scattered in more places
- **selector-pseudo-element-colon-notation: single**, follows our convention

#### For legibility, white space and punctuation
- **Indentation: tab**, so anyone can choose the indent size he or she prefers
- **at-rule-no-unknown: null**
- **color-no-invalid-hex: true**
- **color-hex-length: long**
- **font-family-no-duplicate-names: true**
- **font-family-no-missing-generic-family-keyword: true**
- **font-family-name-quotes**: **always-where-recommended**
- **string-no-newline: true**
- **unit-no-unknown: true**
- **keyframe-declaration-no-important: true**
- **declaration-no-important: true**
- **declaration-block-no-duplicate-properties: [...]**
- **declaration-block-no-shorthand-property-overrides: true**
- **declaration-colon-newline-after**: **always-multi-line**
- **declaration-block-trailing-semicolon**: **always**
- **declaration-colon-space-before**: **never**
- **declaration-colon-space-after**: **always-single-line**
- **declaration-empty-line-before**: **never**
- **declaration-bang-space-before**: **always**
- **declaration-bang-space-after**: **never**
- **declaration-block-single-line-max-declarations**: **1**
- **block-no-empty**: **true**
- **property-no-unknown**: **true**
- **comment-no-empty**: **true**
- **function-calc-no-invalid**: **true**
- **function-calc-no-unspaced-operator**: **true**
- **function-linear-gradient-no-nonstandard-direction**: **true**
- **function-url-quotes**: **always**
- **string-quotes**: **single**
- **value-list-comma-newline-before**: **never-multi-line**
- **value-list-comma-newline-after**: **never-multi-line**
- **value-list-comma-space-after**: **always**
- **selector-pseudo-class-no-unknown**: **true**
- **selector-pseudo-element-no-unknown**: **true**
- **selector-attribute-quotes**: **always**
- **no-empty-first-line**: **true**
- **no-duplicate-selectors**: **true**
- **no-empty-source**: **null**
- **no-descending-specificity**: **true**
- **no-duplicate-at-import-rules**: **true**
- **no-extra-semicolons**: **true**
- **no-invalid-double-slash-comments**: **true**
- **rule-empty-line-before: [...]**
